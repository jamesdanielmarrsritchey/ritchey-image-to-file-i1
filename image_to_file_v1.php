<?php
#Name:Image To File v1
#Description:Convert an image (created using file_to_image_v1) to a file. Returns "TRUE" on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. Image will be saved as PNG.
#Arguments:'file' (required) is a file path for the image to convert. 'destination' is a file path for where to save the new file to. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):file:file:required,$destination:path:required,display_errors:bool:optional
#Content:
if (function_exists('image_to_file_v1') === FALSE){
function image_to_file_v1($file, $destination, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if (@is_file($file) === FALSE){
		$errors[] = 'file';
	}
	if (@is_dir(dirname($destination)) === FALSE){
		$errors[] = 'destination';
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Convert image pixels to data. Treat each pixel RGB color code as 9 byte chunk of data encoded with Ritchey Base Number D. Decode back to data, and save to file.]
	if (@empty($errors) === TRUE){
		###Determine image resolution
		$image = @imagecreatefrompng($file);
		$x = @imagesx($image);
		$y = @imagesy($image);
		###Get each pixel color as RGB color code, unless it is
		$pixel_values = array();
		for ($current_y = 0; $current_y < $y; $current_y++) {
			for($current_x = 0; $current_x < $x; $current_x++) {
				$pixel_value_index = @imagecolorat($image, $current_x, $current_y);
				$pixel_value_rgb_array = @imagecolorsforindex($image, $pixel_value_index);
				$pixel_values[] = $pixel_value_rgb_array['red'];
				$pixel_values[] = $pixel_value_rgb_array['green'];
				$pixel_values[] = $pixel_value_rgb_array['blue'];
			}
		}
		@imagedestroy($image);
		###Decode RGB colors as 3 byte chunks of Ritchey Base Number D data. If segment contains number not between 100-164 discard it, and everything after it (including all future segements).
		foreach ($pixel_values as &$value) {
			if (($value >= 100) && ($value <= 164)){
				#Do nothing
			} else {
				$value = '';
			}
		}
		$pixel_values = @implode($pixel_values);
		$location = realpath(dirname(__FILE__));
		require_once $location . '/dependencies/ritchey_base_number_d_to_data_v1.php';
		$data = @ritchey_base_number_d_to_data_v1($pixel_values, FALSE);
		###Save data to destination file
		@file_put_contents($destination, $data);
		unset($data);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('image_to_file_v1_format_error') === FALSE){
			function image_to_file_v1_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("image_to_file_v1_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return TRUE;
	} else {
		return FALSE;
	}
}
}
?>