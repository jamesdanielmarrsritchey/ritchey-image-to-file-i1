<?php
#Name:Ritchey Base Number D To Data v1
#Description:Convert a Ritchey Base Number D to a data string using the Ritchey Base Number D decoding scheme. Returns string on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'number' is a Ritchey Base Number D. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):number:number:required,display_errors:bool:optional
#Content:
if (function_exists('ritchey_base_number_d_to_data_v1') === FALSE){
function ritchey_base_number_d_to_data_v1($number, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if (@ctype_digit($number) === FALSE){
		$errors[] = "number - not a number";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Convert number to an array of 3 digit sets. Each three digit number is between 100 and 164. Change each 3 digit set into a Base64 character. Convert array to string. Decode Base64. Write data to destination. NOTE: Base64 can encode/decode in chunks of 3 bytes and still achieve the same result as processing data all at once, but this implementation does not make use of this. There is no point since it receives the number as a variable not in a file.]
	if (@empty($errors) === TRUE){
		###Convert number to an array of 3 digit sets
		$number = @str_split($number, 3);
		###Convert each number set to a base64 character.
		foreach ($number as &$character){
			if ($character === "100"){
				$character = 'A';	
			} else if ($character === "101"){
				$character = 'B';
			} else if ($character === "102"){
				$character = 'C';
			} else if ($character === "103"){
				$character = 'D';
			} else if ($character === "104"){
				$character = 'E';
			} else if ($character === "105"){
				$character = 'F';
			} else if ($character === "106"){
				$character = 'G';
			} else if ($character === "107"){
				$character = 'H';
			} else if ($character === "108"){
				$character = 'I';	
			} else if ($character === "109"){
				$character = 'J';	
			} else if ($character === "110"){
				$character = 'K';	
			} else if ($character === "111"){
				$character = 'L';	
			} else if ($character === "112"){
				$character = 'M';	
			} else if ($character === "113"){
				$character = 'N';	
			} else if ($character === "114"){
				$character = 'O';	
			} else if ($character === "115"){
				$character = 'P';	
			} else if ($character === "116"){
				$character = 'Q';	
			} else if ($character === "117"){
				$character = 'R';	
			} else if ($character === "118"){
				$character = 'S';	
			} else if ($character === "119"){
				$character = 'T';	
			} else if ($character === "120"){
				$character = 'U';	
			} else if ($character === "121"){
				$character = 'V';	
			} else if ($character === "122"){
				$character = 'W';	
			} else if ($character === "123"){
				$character = 'X';	
			} else if ($character === "124"){
				$character = 'Y';	
			} else if ($character === "125"){
				$character = 'Z';	
			} else if ($character === "126"){
				$character = 'a';	
			} else if ($character === "127"){
				$character = 'b';
			} else if ($character === "128"){
				$character = 'c';
			} else if ($character === "129"){
				$character = 'd';
			} else if ($character === "130"){
				$character = 'e';
			} else if ($character === "131"){
				$character = 'f';
			} else if ($character === "132"){
				$character = 'g';
			} else if ($character === "133"){
				$character = 'h';
			} else if ($character === "134"){
				$character = 'i';	
			} else if ($character === "135"){
				$character = 'j';	
			} else if ($character === "136"){
				$character = 'k';	
			} else if ($character === "137"){
				$character = 'l';	
			} else if ($character === "138"){
				$character = 'm';	
			} else if ($character === "139"){
				$character = 'n';	
			} else if ($character === "140"){
				$character = 'o';	
			} else if ($character === "141"){
				$character = 'p';	
			} else if ($character === "142"){
				$character = 'q';	
			} else if ($character === "143"){
				$character = 'r';	
			} else if ($character === "144"){
				$character = 's';	
			} else if ($character === "145"){
				$character = 't';	
			} else if ($character === "146"){
				$character = 'u';	
			} else if ($character === "147"){
				$character = 'v';	
			} else if ($character === "148"){
				$character = 'w';	
			} else if ($character === "149"){
				$character = 'x';	
			} else if ($character === "150"){
				$character = 'y';	
			} else if ($character === "151"){
				$character = 'z';	
			} else if ($character === "152"){
				$character = '0';	
			} else if ($character === "153"){
				$character = '1';	
			} else if ($character === "154"){
				$character = '2';	
			} else if ($character === "155"){
				$character = '3';	
			} else if ($character === "156"){
				$character = '4';	
			} else if ($character === "157"){
				$character = '5';	
			} else if ($character === "158"){
				$character = '6';	
			} else if ($character === "159"){
				$character = '7';	
			} else if ($character === "160"){
				$character = '8';	
			} else if ($character === "161"){
				$character = '9';	
			} else if ($character === "162"){
				$character = '+';	
			} else if ($character === "163"){
				$character = '/';	
			} else if ($character === "164"){
				$character = '=';	
			} else {
				$errors[] = "task - Convert numbers to characters";
				goto result;
			}
		}
		###Convert array back to string.
		$number = @implode($number);
		###Decode base64
		$number = @base64_decode($number);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('ritchey_base_number_d_to_data_v1_format_error') === FALSE){
			function ritchey_base_number_d_to_data_v1_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("ritchey_base_number_d_to_data_v1_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return $number;
	} else {
		return FALSE;
	}
}
}
?>